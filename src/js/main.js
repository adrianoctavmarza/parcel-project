console.log("(hacker typing... typing... typing... ENTER) i'm in")

// TOGGLE NAVBAR
const toggleBtn = document.getElementById('toggle')
const navbar = document.getElementById('navbar')
const darkBg = document.getElementById('dark-bg')

toggleBtn.addEventListener('click', () => {
    navbar.classList.toggle('toggle')
    darkBg.classList.toggle('dark-bg')
})